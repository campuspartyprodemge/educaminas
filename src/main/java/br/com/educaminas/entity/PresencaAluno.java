package br.com.educaminas.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "presenca_aluno")
public class PresencaAluno
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer   id;

	@Column
	@Temporal(TemporalType.DATE)
	private Date      dia;

	@Column
	private Character status;

	@JoinColumn(name = "aluno_id", referencedColumnName = "id")
	@ManyToOne
	private Aluno     aluno;

	@JoinColumn(name = "bimestre_id", referencedColumnName = "id")
	@ManyToOne
	private Bimestre  bimestre;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Aluno getAluno()
	{
		return aluno;
	}

	public void setAluno(Aluno aluno)
	{
		this.aluno = aluno;
	}

	public Date getDia()
	{
		return dia;
	}

	public void setDia(Date dia)
	{
		this.dia = dia;
	}

	public Character getStatus()
	{
		return status;
	}

	public void setStatus(Character status)
	{
		this.status = status;
	}

	public Bimestre getBimestre()
	{
		return bimestre;
	}

	public void setBimestre(Bimestre bimestre)
	{
		this.bimestre = bimestre;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		PresencaAluno other = (PresencaAluno) obj;
		if(id == null)
		{
			if(other.id != null) return false;
		}
		else if(!id.equals(other.id)) return false;
		return true;
	}

}