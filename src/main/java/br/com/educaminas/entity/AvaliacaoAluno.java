package br.com.educaminas.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "avaliacao_aluno")
public class AvaliacaoAluno
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer   id;

	@Column(name = "data_avaliacao")
	@Temporal(TemporalType.DATE)
	private Date      data;

	@Column
	private int       nota;

	@Column
	private Character status;

	@JoinColumn(name = "aluno_id", referencedColumnName = "id")
	@ManyToOne
	private Aluno     aluno;

	@JoinColumn(name = "avaliacao_id", referencedColumnName = "id")
	@ManyToOne
	private Avaliacao avaliacao;

	@JoinColumn(name = "bimestre_id", referencedColumnName = "id")
	@ManyToOne
	private Bimestre  bimestre;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Aluno getAluno()
	{
		return aluno;
	}

	public void setAluno(Aluno aluno)
	{
		this.aluno = aluno;
	}

	public Date getData()
	{
		return data;
	}

	public void setData(Date data)
	{
		this.data = data;
	}

	public int getNota()
	{
		return nota;
	}

	public void setNota(int nota)
	{
		this.nota = nota;
	}

	public Avaliacao getAvaliacao()
	{
		return avaliacao;
	}

	public void setAvaliacao(Avaliacao avaliacao)
	{
		this.avaliacao = avaliacao;
	}

	public Character getStatus()
	{
		return status;
	}

	public void setStatus(Character status)
	{
		this.status = status;
	}

	public Bimestre getBimestre()
	{
		return bimestre;
	}

	public void setBimestre(Bimestre bimestre)
	{
		this.bimestre = bimestre;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		AvaliacaoAluno other = (AvaliacaoAluno) obj;
		if(id == null)
		{
			if(other.id != null) return false;
		}
		else if(!id.equals(other.id)) return false;
		return true;
	}

}