package br.com.educaminas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "materias")
public class Materia
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column
	private String  nome;

	@Column
	private boolean check;

	@JoinColumn(name = "curso_id", referencedColumnName = "id")
	@ManyToOne
	private Curso   curso;

	public Materia()
	{
		super();
	}

	public Materia(String nome, boolean check, Curso curso)
	{
		super();
		this.nome = nome;
		this.check = check;
		this.curso = curso;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public boolean isCheck()
	{
		return check;
	}

	public void setCheck(boolean check)
	{
		this.check = check;
	}

	public Curso getCurso()
	{
		return curso;
	}

	public void setCurso(Curso curso)
	{
		this.curso = curso;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		Materia other = (Materia) obj;
		if(id == null)
		{
			if(other.id != null) return false;
		}
		else if(!id.equals(other.id)) return false;
		return true;
	}

}
