package br.com.educaminas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "avaliacoes")
public class Avaliacao
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer     id;

	@Column
	private String      nome;

	@JoinColumn(name = "classe_id", referencedColumnName = "id")
	@ManyToOne
	private AlunoClasse classe;

	public Avaliacao()
	{
		super();
	}

	public Avaliacao(String nome, AlunoClasse classe)
	{
		super();
		this.nome = nome;
		this.classe = classe;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public AlunoClasse getClasse()
	{
		return classe;
	}

	public void setClasse(AlunoClasse classe)
	{
		this.classe = classe;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		Avaliacao other = (Avaliacao) obj;
		if(id == null)
		{
			if(other.id != null) return false;
		}
		else if(!id.equals(other.id)) return false;
		return true;
	}

}