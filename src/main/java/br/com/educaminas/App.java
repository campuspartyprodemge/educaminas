package br.com.educaminas;

import java.util.ArrayList;
import java.util.List;

import br.com.educaminas.entity.Aluno;
import br.com.educaminas.entity.AlunoClasse;
import br.com.educaminas.entity.Bimestre;
import br.com.educaminas.entity.Classe;
import br.com.educaminas.entity.Curso;
import br.com.educaminas.entity.Materia;
import br.com.educaminas.entity.Professor;
import br.com.educaminas.entity.Serie;

/**
 * Hello world!
 *
 */
public class App
{
	public static void main(String[] args)
	{

		String[] bimestres = {"1ª Bimestre", "2ª Bimestre", "3ª Bimestre", "4ª Bimestre"};
		List<Bimestre> listBimestre = new ArrayList<Bimestre>();
		for(int i = 0; i < bimestres.length; i++)
		{
			Bimestre bimestre = new Bimestre(bimestres[i]);
			listBimestre.add(bimestre);
		}

		String[] series = {"1ª Ano", "2ª Ano", "3ª Ano"};
		List<Serie> listSerie = new ArrayList<Serie>();
		for(int i = 0; i < series.length; i++)
		{
			Serie serie = new Serie(series[i]);
			listSerie.add(serie);
		}

		String[] cursos = {"Matématica"};
		List<Curso> listCurso = new ArrayList<Curso>();
		for(int i = 0; i < cursos.length; i++)
		{
			for(Serie serie : listSerie)
			{
				Curso curso = new Curso(cursos[i], serie);
				listCurso.add(curso);
			}
		}

		// MATERIAS
		for(Curso curso : listCurso)
		{
			for(int i = 1; i <= 30; i++)
			{
				Materia materia = new Materia("Materia " + i, false, curso);
			}
		}

		Professor professor = new Professor("Professor Pascal", "111.111.111-11");

		List<Aluno> listAlunos = new ArrayList<Aluno>();
		for(int i = 1; i <= 30; i++)
		{
			Aluno aluno = new Aluno("Aluno " + i);
			listAlunos.add(aluno);
		}

		// CLASSES
		List<Classe> listClasse = new ArrayList<Classe>();
		for(Curso curso : listCurso)
		{
			Classe classe = new Classe(professor, curso);
			listClasse.add(classe);
		}

		// ALUNOS CLASSES
		for(Classe classe : listClasse)
		{
			for(Aluno aluno : listAlunos)
			{
				AlunoClasse alunoClasse = new AlunoClasse(aluno, classe);
			}
		}

	}
}
